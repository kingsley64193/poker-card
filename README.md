## Clone the project

Run `git clone git@bitbucket.org:kingsley64193/poker-card.git`

## Navigate into the project

Run `cd poker-card`

## Install depencencies

Run `npm install or npm i`

## Run the application

Run `curl http://localhost:3000` or `Send GET request from Postman with url http://localhost:3000`
