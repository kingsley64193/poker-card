const express = require('express');
const router = express.Router();
const PokerHand = require('poker-hand-evaluator');


/* Get cards shuffle page. */
router.get('/', (req, res) => {
  const suits = ["Spades", "Diamonds", "Club", "Heart"];
  const values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];

  // empty array to contain cards
  let deck = [];

  // create a deck of cards
  for (let i = 0; i < suits.length; i++) {
    for (let x = 0; x < values.length; x++) {
      let card = {
        Value: values[x],
        Suit: suits[i]
      };
      deck.push(card);
    }
  }

  // shuffle the cards
  for (let i = deck.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * i);
    let temp = deck[i];
    deck[i] = deck[j];
    deck[j] = temp;
  }

  // display 5 results
  const cards = [];
  for (let i = 0; i < 5; i++) {
    cards.push({
      deckValue: deck[i].Value,
      deckSuite: deck[i].Suit
    })

  }

  res.send(detectTheHand(cards));
});

// Evaluating the player's hand
const detectTheHand = (cards) => {
  const myCards = cards.filter(v => v).map(v => v.deckValue.concat(v.deckSuite[0]));
  const cvtMyCards = myCards.toString()
  const replaceString = cvtMyCards.replace(/,/g, ' ');
try{
  const myPokerHand = new PokerHand(replaceString);
  console.log('Your hand :', myPokerHand.hand);
  console.log('Your have :', myPokerHand.getRank());
  console.log('Your score :', myPokerHand.score);
  return myPokerHand.describe() || [];
} catch(err){
  return 'Oops, Incorrect hand, Please shuffle again';
}




}


module.exports = router;